select count(*)
from consumercomplaints
group by month(`Date_Received`);

-- --------------------------------------------------------------------------

select *
from consumercomplaints
where Tags = 'Older American';

-- --------------------------------------------------------------------------

SELECT company, SUM( CASE WHEN `Company_Response_to_Consumer` = 'Closed' THEN 1 ELSE 0 END ) AS `Closed`, SUM( CASE WHEN `Company_Response_to_Consumer` = 'Closed with explanation' THEN 1 ELSE 0 END ) AS `Closed with explanation`, SUM( CASE WHEN `Company_Response_to_Consumer` = 'Closed with non-monetary relief' THEN 1 ELSE 0 END ) AS `Closed with non-monetary relief` FROM consumercomplaints WHERE `Company_Response_to_Consumer` IN( 'Closed', 'Closed with explanation', 'Closed with non-monetary relief' ) GROUP BY company